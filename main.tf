module "vpc" {
  source = "./modules/vpc"
  prefix = var.prefix
  subnets_quantity = var.subnets_quantity
  vpc_cidr_block = var.vpc_cidr_block
}

module "eks" {
  source = "./modules/eks"
  prefix = var.prefix
  cluster_name = var.cluster_name
  vpc_id = module.vpc.vpc_id
  subnet_ids = module.vpc.subnet_ids
  logs_retetion_in_days = var.logs_retetion_in_days
  depends_on = [
    module.vpc,
  ]
}

module "ec2" {
  source = "./modules/ec2"
  prefix = var.prefix
  cluster_name = var.cluster_name
  subnet_ids = module.vpc.subnet_ids
  eks_cluster_name = module.eks.eks_cluster_name
  scaling_disered_size = var.scaling_disered_size
  scaling_max_size = var.scaling_max_size
  scaling_min_size = var.scaling_min_size
  depends_on = [
    module.vpc,
    module.eks,
  ]
}
