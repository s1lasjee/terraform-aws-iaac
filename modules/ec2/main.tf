#################################################
##                  NODE ROLE                  ##
#################################################

resource "aws_iam_role" "node" {
  name = "${var.prefix}-${var.cluster_name}-role-node"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect   = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
        Action = [
          "sts:AssumeRole"
        ]
      },
    ]
  })
}

##################################################
##                 NODE POLICYS                 ##
##################################################

resource "aws_iam_role_policy_attachment" "node-worker-policy" {
  role = aws_iam_role.node.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}

resource "aws_iam_role_policy_attachment" "node-eks-cni-policy" {
  role = aws_iam_role.node.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}

resource "aws_iam_role_policy_attachment" "node-container-registry-policy" {
  role = aws_iam_role.node.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

###################################################
##                     NODES                     ##
###################################################

resource "aws_eks_node_group" "node-1" {
  cluster_name = var.eks_cluster_name
  node_group_name = "public-nodes"
  instance_types = ["t3.micro"]
  node_role_arn = aws_iam_role.node.arn
  subnet_ids = var.subnet_ids
  scaling_config {
    desired_size = var.scaling_disered_size
    max_size = var.scaling_max_size
    min_size = var.scaling_min_size
  }
  depends_on = [
    aws_iam_role_policy_attachment.node-worker-policy,
    aws_iam_role_policy_attachment.node-eks-cni-policy,
    aws_iam_role_policy_attachment.node-container-registry-policy,
  ]
}
