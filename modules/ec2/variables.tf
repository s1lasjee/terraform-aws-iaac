
variable "prefix" {}

variable "subnet_ids" {}

variable scaling_disered_size {}

variable scaling_max_size {}

variable scaling_min_size {}

variable "cluster_name" {}

variable "eks_cluster_name" {}
