####################################################
##                  CLUSTER ROLE                  ##
####################################################

resource "aws_iam_role" "cluster" {
  name = "${var.prefix}-${var.cluster_name}-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect   = "Allow"
        Principal = {
          Service = "eks.amazonaws.com"
        }
        Action = [
          "sts:AssumeRole"
        ]
      },
    ]
  })
}

###################################################
##                CLUSTER POLICYS                ##
###################################################

resource "aws_iam_role_policy_attachment" "cluster-policy-controller" {
  role = aws_iam_role.cluster.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
}

resource "aws_iam_role_policy_attachment" "cluster-policy" {
  role = aws_iam_role.cluster.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

##################################################
##                SECURITY GROUP                ##
##################################################

resource "aws_security_group" "sg" {
  vpc_id = var.vpc_id
  egress {
    to_port = 0
    from_port = 0
    protocol = "-1"
    prefix_list_ids = []
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.prefix}-sg"
  }
}

##################################################
##                     LOGS                     ##
##################################################

resource "aws_cloudwatch_log_group" "cluster-logs" {
  name = "/aws/eks/${var.prefix}-${var.cluster_name}/cluster"
  retention_in_days = var.logs_retetion_in_days
}

###################################################
##                    CLUSTER                    ##
###################################################

resource "aws_eks_cluster" "eks-cluster" {
  name = "${var.prefix}-${var.cluster_name}"
  role_arn = aws_iam_role.cluster.arn
  enabled_cluster_log_types = ["api", "audit"]
  vpc_config {
    subnet_ids = var.subnet_ids
    security_group_ids = [aws_security_group.sg.id]
  }
  depends_on = [
    aws_cloudwatch_log_group.cluster-logs,
    aws_iam_role_policy_attachment.cluster-policy-controller,
    aws_iam_role_policy_attachment.cluster-policy,
  ]
}
