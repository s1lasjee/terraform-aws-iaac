#################################################
##                     VPC                     ##
#################################################

resource "aws_vpc" "public-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Environment = "Development"
    Name = "${var.prefix}-vpc"
  }
}

#################################################
##                   SUBNETS                   ##
#################################################

data "aws_availability_zones" "zones" {}

resource "aws_subnet" "public-subnets" {
  count = var.subnets_quantity
  map_public_ip_on_launch = true
  availability_zone = data.aws_availability_zones.zones.names[count.index]
  vpc_id = aws_vpc.public-vpc.id
  cidr_block = "10.0.${count.index}.0/24"
  tags = {
    Name = "${var.prefix}-subnet-${count.index}"
  }
}

##################################################
##               INTERNET GATEWAY               ##
##################################################

resource "aws_internet_gateway" "main-igw" {
  vpc_id = aws_vpc.public-vpc.id
  tags = {
    Name = "${var.prefix}-igw"
  }
}

##################################################
##                 ROUTE TABLES                 ##
##################################################

resource "aws_route_table" "main-rtb" {
  vpc_id = aws_vpc.public-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-igw.id
  }
  tags = {
    Name = "${var.prefix}-rtb"
  }
}

resource "aws_route_table_association" "main-rtb-association" {
  count = var.subnets_quantity
  route_table_id = aws_route_table.main-rtb.id
  subnet_id = aws_subnet.public-subnets.*.id[count.index]
}
