output "vpc_id" {
  value = aws_vpc.public-vpc.id
}

output "subnet_ids" {
  value = aws_subnet.public-subnets[*].id
}
