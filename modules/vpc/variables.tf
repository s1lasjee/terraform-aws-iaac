variable "prefix" {
  type = string
  description = "Prefix to assign in resource names"
}
variable "vpc_cidr_block" {
  type = string
  description = ""
}

variable "subnets_quantity" {
  type = number
  default = 1
  description = "Number of subnets that will be created for the VPC"
}
